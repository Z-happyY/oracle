

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

学号：202010414326    姓名：张俊瑶

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|



## 实验步骤

### 连接数据库

![image-20230526112220622](pict1.png)

### 创建表空间

创建两个表空间：一个叫做sale_data，用于存储销售数据；另一个叫做inventory_data，用于存储库存数据。

#### sale_data表空间

```sql
CREATE TABLESPACE sale_data
DATAFILE '/home/oracle/app/oracle/oradata/orcl/sale_data.dbf'
SIZE 100M AUTOEXTEND ON NEXT 10M MAXSIZE UNLIMITED;
```

#### inventory_data表空间

```sql
CREATE TABLESPACE inventory_data
DATAFILE '/u01/app/oracle/oradata/dbname/inventory_data.dbf'
SIZE 100M AUTOEXTEND ON NEXT 10M MAXSIZE UNLIMITED;
```

![image-20230526113007927](pict2.png) 

#### 表空间设计

| 表空间名称     | 表名称             | 表描述               |
| -------------- | :----------------- | -------------------- |
| sale_data      | product            | 商品表               |
| sale_data      | sales_order        | 销售订单表           |
| sale_data      | customer           | 客户表               |
| sale_data      | sales_order_detail | 销售订单明细表       |
| inventory_data | supplier           | 供应商表             |
| inventory_data | inventory          | 存储各个表的相关索引 |

### 创建表结构

#### 创建商品表

```sql
CREATE TABLE product (
  id NUMBER(10) PRIMARY KEY,
  name VARCHAR2(100),
  description VARCHAR2(1000),
  price NUMBER(10,2),
  supplier_id NUMBER(10) REFERENCES supplier(id)
) TABLESPACE sale_data;
```

![image-20230526114838239](pict5.png) 

#### 创建销售订单表

```sql
CREATE TABLE sales_order (
  id NUMBER(10) PRIMARY KEY,
  customer_id NUMBER(10) REFERENCES customer(id),
  order_date DATE,
  status VARCHAR2(20),
  total_amount NUMBER(10,2)
) TABLESPACE sale_data;
```

![image-20230526114943700](pict6.png) 

#### 创建订单明细表

```sql
CREATE TABLE sales_order_detail (
  id NUMBER(10) PRIMARY KEY,
  sales_order_id NUMBER(10) REFERENCES sales_order(id),
  product_id NUMBER(10) REFERENCES product(id),
  quantity NUMBER(10),
  unit_price NUMBER(10,2),
  amount NUMBER(10,2)
) TABLESPACE sale_data;
```

![image-20230526115031370](pict7.png) 

#### 创建客户表

```sql
CREATE TABLE customer (
  id NUMBER(10) PRIMARY KEY,
  name VARCHAR2(100),
  address VARCHAR2(200),
  phone_number VARCHAR2(20),
  email VARCHAR2(100)
) TABLESPACE sale_data;
```

![image-20230526114640737](pict3.png) 

#### 创建供应商表

```sql
CREATE TABLE supplier (
  id NUMBER(10) PRIMARY KEY,
  name VARCHAR2(100),
  address VARCHAR2(200),
  phone_number VARCHAR2(20),
  email VARCHAR2(100)
) TABLESPACE inventory_data;
```

![image-20230526115117898](pict8.png) 

#### 创建库存表

```sql
CREATE TABLE inventory (
  id NUMBER(10) PRIMARY KEY,
  product_id NUMBER(10) REFERENCES product(id),
  supplier_id NUMBER(10) REFERENCES supplier(id),
  quantity NUMBER(10)
) TABLESPACE inventory_data;
```

![image-20230526114747989](pict4.png) 

### 创建用户并进行权限分配

```sql
-- 销售系统用户sales
CREATE USER sales IDENTIFIED BY 123456;
GRANT CREATE SESSION TO sales;
GRANT UNLIMITED TABLESPACE TO sales;
GRANT SELECT, INSERT, UPDATE, DELETE ON product TO sales;
GRANT SELECT, INSERT, UPDATE, DELETE ON sales_order TO sales;
GRANT SELECT, INSERT, UPDATE, DELETE ON sales_order_detail TO sales;
GRANT SELECT, INSERT, UPDATE, DELETE ON customer TO sales;

-- 库存管理系统用户inventory
CREATE USER inventory IDENTIFIED BY 123456;
GRANT CREATE SESSION TO inventory;
GRANT UNLIMITED TABLESPACE TO inventory;
GRANT SELECT, INSERT, UPDATE, DELETE ON supplier TO inventory;
GRANT SELECT, INSERT, UPDATE, DELETE ON inventory TO inventory;

```

![image-20230526115439354](pict9.png) 

### 设计PL/SQL，创建存储过程和函数

#### 设计存储过程填充表

##### 填充供应商表

```sql
CREATE OR REPLACE PROCEDURE insert_supplier_data IS
BEGIN
    FOR i IN 1..2000 LOOP
        INSERT INTO supplier (id, name, address, phone_number, email)
        VALUES (i, '供应商' || i, '地址' || i, '电话号码' || i, '邮箱' || i || '@example.com');
    END LOOP;
    COMMIT;
END;
```

![image-20230526120150029](pict10.png) 

##### 填充客户表

```sql
CREATE OR REPLACE PROCEDURE insert_customer_data IS
BEGIN
    FOR i IN 1..30000 LOOP
        INSERT INTO customer (id, name, address, phone_number, email)
        VALUES (i, '客户' || i, '地址' || i, '电话号码' || i, '邮箱' || i || '@example.com');
    END LOOP;
    COMMIT;
END;

```

![image-20230526120336372](pict11.png) 

##### 填充商品表

```sql
CREATE OR REPLACE PROCEDURE insert_product_data IS
BEGIN
    FOR i IN 1..30000 LOOP
        INSERT INTO product (id, name, description, price, supplier_id)
        VALUES (i, '商品' || i, '商品' || i || ' 的描述信息', ROUND(DBMS_RANDOM.VALUE(1, 1000), 2), ROUND(DBMS_RANDOM.VALUE(1, 2000)));
    END LOOP;
    COMMIT;
END;
```

![image-20230526120504662](pict12.png) 

##### 填充销售订单表

```sql
CREATE OR REPLACE PROCEDURE insert_sales_order_data IS
BEGIN
    FOR i IN 1..30000 LOOP
        INSERT INTO sales_order (id, customer_id, order_date, status, total_amount)
        VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 5000)), SYSDATE, CASE WHEN MOD(i, 2) = 0 THEN '已发货' ELSE '待发货' END, ROUND(DBMS_RANDOM.VALUE(100, 10000), 2));
    END LOOP;
    COMMIT;
END;

```

![image-20230526120655960](pict13.png) 

##### 填充销售订单详情表

```sql
CREATE OR REPLACE PROCEDURE insert_sales_order_detail_data IS
BEGIN
    FOR i IN 1..30000 LOOP
        INSERT INTO sales_order_detail (id, sales_order_id, product_id, quantity, unit_price, amount)
        VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 30000)), ROUND(DBMS_RANDOM.VALUE(1, 30000)), ROUND(DBMS_RANDOM.VALUE(1, 10)), ROUND(DBMS_RANDOM.VALUE(10, 100), 2), ROUND(DBMS_RANDOM.VALUE(100, 1000), 2));
    END LOOP;
    COMMIT;
END;

```

![image-20230526120828598](pict14.png) 

##### 填充库存表

```sql
CREATE OR REPLACE PROCEDURE insert_inventory_data IS
BEGIN
    FOR i IN 1..30000 LOOP
        INSERT INTO inventory (id, product_id, supplier_id, quantity)
        VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 30000)), ROUND(DBMS_RANDOM.VALUE(1, 2000)), ROUND(DBMS_RANDOM.VALUE(1, 100)));
    END LOOP;
    COMMIT;
END;

```

![image-20230526120944949](pict15.png) 

####  创建程序包，实现相关存储过程和函数

##### 查询每个客户的订单总金额

```sql
CREATE OR REPLACE PACKAGE order_amount_pkg IS
    -- 计算每个客户的订单总金额
    FUNCTION get_order_amount_by_customer RETURN SYS_REFCURSOR;
END order_amount_pkg;

CREATE OR REPLACE PACKAGE BODY order_amount_pkg IS
    FUNCTION get_order_amount_by_customer RETURN SYS_REFCURSOR IS
        v_cursor SYS_REFCURSOR;
    BEGIN
        OPEN v_cursor FOR
            SELECT c.name, SUM(so.total_amount) AS total_amount
            FROM customer c
            INNER JOIN sales_order so ON c.id = so.customer_id
            GROUP BY c.id, c.name;
        RETURN v_cursor;
    END get_order_amount_by_customer;
END order_amount_pkg;
```

说明：该程序包中定义了一个get_order_amount_by_customer函数，通过联结客户表和销售订单表计算每个客户的订单总金额，并以游标形式返回。

##### 查询每个商品的销售总量和销售总额

```sql
CREATE OR REPLACE PACKAGE product_sale_pkg IS
    -- 查询每个商品的销售总量和销售总额
    FUNCTION get_product_sale_info RETURN SYS_REFCURSOR;
END product_sale_pkg;

CREATE OR REPLACE PACKAGE BODY product_sale_pkg IS
    FUNCTION get_product_sale_info RETURN SYS_REFCURSOR IS
        v_cursor SYS_REFCURSOR;
    BEGIN
        OPEN v_cursor FOR
            SELECT p.name, SUM(sod.quantity) AS total_quantity, SUM(sod.amount) AS total_amount
            FROM product p
            INNER JOIN sales_order_detail sod ON p.id = sod.product_id
            GROUP BY p.id, p.name;
        RETURN v_cursor;
    END get_product_sale_info;
END product_sale_pkg;
```

说明：该程序包中定义了一个get_product_sale_info函数，通过联结商品表和销售订单明细表计算每个商品的销售总量和销售总额，并以游标形式返回。

##### 根据客户ID查询客户购买的商品及其数量

```sql
CREATE OR REPLACE PACKAGE customer_purchase_pkg IS
    -- 根据客户ID查询客户购买的商品及其数量
    FUNCTION get_customer_purchase_info(p_customer_id IN customer.id%TYPE) RETURN SYS_REFCURSOR;
END customer_purchase_pkg;

CREATE OR REPLACE PACKAGE BODY customer_purchase_pkg IS
    FUNCTION get_customer_purchase_info(p_customer_id IN customer.id%TYPE) RETURN SYS_REFCURSOR IS
        v_cursor SYS_REFCURSOR;
    BEGIN
        OPEN v_cursor FOR
            SELECT p.name, sod.quantity
            FROM customer c
            INNER JOIN sales_order so ON c.id = so.customer_id
            INNER JOIN sales_order_detail sod ON so.id = sod.sales_order_id
            INNER JOIN product p ON sod.product_id = p.id
            WHERE c.id = p_customer_id;
        RETURN v_cursor;
    END get_customer_purchase_info;
END customer_purchase_pkg;
```

说明：该程序包中定义了一个get_customer_purchase_info函数，通过联结客户表、销售订单表、销售订单明细表和商品表，根据客户ID查询客户购买的商品及其数量，并以游标形式返回。

##### 根据订单状态查询订单详情

```sql
CREATE OR REPLACE PACKAGE sales_order_pkg IS
    -- 根据订单状态查询订单详情
    FUNCTION get_sales_order_detail_by_status(p_status IN sales_order.status%TYPE) RETURN SYS_REFCURSOR;
END sales_order_pkg;

CREATE OR REPLACE PACKAGE BODY sales_order_pkg IS
    FUNCTION get_sales_order_detail_by_status(p_status IN sales_order.status%TYPE) RETURN SYS_REFCURSOR IS
        v_cursor SYS_REFCURSOR;
    BEGIN
        OPEN v_cursor FOR
            SELECT c.name AS customer_name, so.order_date, p.name AS product_name, sod.quantity, sod.unit_price, sod.amount
            FROM sales_order so
            INNER JOIN customer c ON so.customer_id = c.id
            INNER JOIN sales_order_detail sod ON so.id = sod.sales_order_id
            INNER JOIN product p ON sod.product_id = p.id
            WHERE so.status = p_status;
        RETURN v_cursor;
    END get_sales_order_detail_by_status;
END sales_order_pkg;
```

说明：该程序包中定义了一个get_sales_order_detail_by_status函数，通过联结客户表、销售订单表、销售订单明细表和商品表，根据订单状态查询订单详情，并以游标形式返回。

通过以上程序包的设计和实现，我们可以更方便地进行数据查询和业务处理。

![image-20230526122439181](pict16.png)

### 数据库备份方案

可以考虑使用RMAN（Recovery Manager）工具进行备份。

创建密码文件

```
$ orapwd file=/home/oracle/app/oracle/product/18.0.0/dbhome_1/dbs/orapwpdb_zjy password=123456
```

修改RMAN参数文件

```
$ vi /home/oracle/app/oracle/product/18.0.0/dbhome_1/network/admin/rmanpdb_zjy.par
```

在文件中添加以下内容：

```
target database pdb_zjy
catalog start with 'rman_catalog'
nocatalog
connect target sys/123456@pdb_zjy
connect catalog rman_catalog/rman_password@rman_database_name
```

连接到数据库

```
$ rman target /
```

连接到RMAN库

```
$ rman catalog rman_catalog/rman_password@rman_database_name
```

执行备份

全量备份：

```
run {
  allocate channel c1 device type disk format '/home/oracle/app/oracle/backup/%U';
  backup database plus archivelog;
}
```

差异备份：

```
run {
  allocate channel c1 device type disk format '/home/oracle/app/oracle/backup/%U';
  backup incremental level 1 database plus archivelog;
}
```

日志备份：

```
run {
  allocate channel c1 device type disk format '/home/oracle/app/oracle/backup/%U';
  backup archivelog all delete input;
}
```

控制文件备份：

```
run {
  allocate channel c1 device type disk format '/home/oracle/app/oracle/backup/%U';
  backup current controlfile;
}
```

定时执行备份

为了实现每天自动执行全量备份，使用crontab，具体操作如下：

1）编辑crontab文件，添加一行定时任务：

```
$ crontab -e
```

在文件底部添加以下内容：

```
0 0 * * * /path/to/rman_backup_script.sh >/dev/null 2>&1
```

该命令表示每天凌晨0点执行rman_backup_script.sh脚本。

2）创建备份脚本：

```
$ vi /home/oracle/rman_backup_script.sh
```

在文件中添加以下内容：

```
#!/bin/bash
ORACLE_SID=pdb_zjy
export ORACLE_SID
rman target / catalog rman_catalog/rman_password@rman_database_name << EOF
run {
  allocate channel c1 device type disk format '/home/oracle/app/oracle/backup/%U';
  backup database plus archivelog;
}
EOF
```

执行以下命令，为脚本添加可执行权限：

```
$ chmod +x /home/oracle/rman_backup_script.sh
```









